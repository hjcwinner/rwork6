const express = require('express') //시작은 무조건 require
const bodyParser = require('body-parser')  ///body(post나 put값 읽기)
const morgan = require('morgan')  //////통신 로그찍기
const mongoose = require('mongoose')  /////database를 연결하기 위한 함수


const app = express();
const productRoutes = require("./routes/product") //모듈화한거 불러오기


/////database
database = ("mongodb+srv://homework:hjc8056107@cluster0.uf2j0.mongodb.net/home?retryWrites=true&w=majority")
mongoose
    .connect(database,{ useNewUrlParser: true, useUnifiedTopology: true})
    .then(() => console.log("연결잘됨"))
    .catch(err => console.log(err.message))


/////middlewere
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:false}))
app.use(morgan("dev"))




app.use('/product', productRoutes)
const port = 7070
app.listen(port, console.log("서버시작됨"))
