const express = require('express') ///시작은 무조건require
const router = express.Router() ///Router은 무조건 앞에대문자
const bodyParser = require('body-parser')
const productcom = require('../model/product');

/////불러오기 get
router.get('/', (req, res) => {
    productcom
        .find()
        .then(doc => {
            res.json({
                message : "allproduc",
                result : doc
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
});

/////1개불러오기 get
router.get('/:id', (req, res) => {
    productcom
        .findById(req.params.id) ////id를 추출한다.
        .then(docone => {
            res.json({
                message : "1개완료",
                result : docone
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
    
});


//////등록하기  post
router.post('/', (req, res) => {
     const product = new productcom({
        thname : req.body.namename,  ////여기 가장 오른쪽에 키값을 꼭넣어라
        thprice : req.body.priceprice   ////여기 가장 오른쪽에 키값을 꼭넣어라
     })

        product
        .save()
        .then(savepo => {
            res.json({
                message : "저장완료",
                productInfo : savepo
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
});
/////수정하기 patch
router.patch('/', (req, res) => {
    res.json({
        message : "수정하기"
    });
});
/////삭제하기 delete
router.delete('/:id', (req, res) => {
    productcom
        .findByIdAndDelete(req.params.id)
        .then(() => {
            res.json({
                message : "삭제완료"
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })    
});
module.exports = router //////상수화된 router을 모듈화